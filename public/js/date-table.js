$(document).ready(function () {

    tableHideShow();
    submitDate();
    $('#start').click(function () {
        var data = {'status': 'start'};
        $.post("save_hours",
            data, []
        );
        reloadWindow();
    });
    $('#stop').click(function () {
        var data = {'status': 'stop'};
        $.post("save_hours",
            data, []
        );
        reloadWindow();
    })
    ;

    function tableHideShow() {
        $('.Show').click(function () {
            $('.usersBoard').toggle();
        });
    }

    function reloadWindow() {
        location.reload(true);
    }

    function submitDate() {
        $('#month').on('input', function () {
            $('.dateSubmit').trigger('click')
        });
        $('#year').on('input', function () {
            $('.dateSubmit').trigger('click')
        })
    }
});


