$(document).ready(function () {

    $('#late_time').change(function () {

        $.post('admin/lateUser', data, 'json');
    });
    $('#time_check_late').change(function () {
        var result = $('#time_check_late').val().match(/^[0-9][0-9]:[0-9][0-9]$/);
        if (result !== null) {
            var time = result.join() + ':00';
            var data = {'time': time};
            var check_time = time.split(':');
            if (check_time['0'] <= 23 && check_time['1'] <= 59) {
                $.post('newStartJob', data, function (data) {
                    $('.lateUsers').empty();
                    getDataLateUsers(data);
                    console.log(data);
                }, 'json');
            } else {
                alert('Время не должно превышать 23 часа и 59 мин!!!')
            }
        } else {
            alert(' Вы ввели неправельный формат времени! попробуйте еще раз')
        }
    });

    $.get("lateUser", function (data) {
        getDataLateUsers(data);
    }, 'json');
});

function getDataLateUsers(users) {
    var lateUsersDiv = $('.lateUsers');
    $.each(users, function (key, user) {
        var name = user['user']['name'];
        var start_time = user['time'];
        var difference = user['difference'];
        var result = "<div class='user_late'><p>Имя :" + name + "<br>Начало работы :" + start_time + "<br><span class='late'>опаздал на :" + difference + "</span></p></div>";
        lateUsersDiv.append(result);
    })
}