$(document).ready(function () {
    $('.staffs_time').append($('.session-staff'));
    $('.all_staffs').click(function () {
        $('.users_table').toggle();

        $.get("admin/users", function (data) {
            for (var i = 0; i <= data.length; i++) {
                $('.staffs').append(
                    "<tr><td> " + data[i]['name'] + "</td>" +
                    "<td><a href='admin/user_edit/" + data[i]['id'] + "'>редактировать</a></td>"
                    + "<td><a href='admin/user_remove/" + data[i]['id'] + "'>Удалить</a>" +
                    "</td>" + "</tr>");
            }
        }, 'json');
    });
    search();
    $(".start_time").change(function () {
        var id = $(this).attr('id');
        var start_time = $(this).val();
        var data = {'status': 'start_time', 'id': id, 'start_time': start_time};
        $.post('changeTime', data, []);
    });
    $(".stop_time").change(function () {
        var id = $(this).attr('id');
        var stop_time = $(this).val();
        var data = {'status': 'stop_time', 'id': id, 'stop_time': stop_time};
        $.post('changeTime', data, []);
    });
    addHolidaysCount();
    search();

});

function search() {
    $('#searchName').change(function () {
        if ($(this).val()) {
            var name = $('[data-name*="' + $(this).val() + '"]');
            $('td').removeClass();
            $('td').addClass('false');
            name.removeClass();
            name.addClass('active');
            $('.false').hide();
            $('.active').show();
        } else {
            reloadWindow()
        }

    });

    $('.repeat').click(function () {
        var checked = $(this).attr('checked');
        var id = $(this).attr('id');
        if (checked === 'checked') {
            $.post('makeActiveHoliday', {'id': id, 'checked': 'true'}, []);
        } else {
            $.post('makeActiveHoliday', {'id': id, 'checked': 'false'}, []);
        }
        reloadWindow()
    });
}


function reloadWindow() {
    location.reload(true);
}