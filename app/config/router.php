<?php

$router = $di->getRouter();

// Define your routes here
$router->add('/', [
    'controller' => 'index',
    'action' => 'index'
]);
$router->add('/log-in', [
    'controller' => 'index',
    'action' => 'start'
])->setName('log-in');
$router->add('/admin/createUser', [
    'controller' => 'admin',
    'action' => 'userCreate'
]);
$router->add('/admin', [
    'controller' => 'admin',
    'action' => 'index'
]);
$router->add('/admin/users', [
    'controller' => 'admin',
    'action' => 'users'
]);
$router->add('/admin/user_edit/{id}', [
    'controller' => 'admin',
    'action' => 'editUser'
]);
$router->add('/save_hours', [
    'controller' => 'index',
    'action' => 'hoursSave'
]);
$router->add('/admin/user_remove/{id}', [
    'controller' => 'admin',
    'action' => 'remove'
]);
$router->add('/getSimpleHours', [
    'controller' => 'index',
    'action' => 'getSimpleHoursForSessionUser'
]);
$router->add('/checkStatus', [
    'controller' => 'index',
    'action' => 'checkStatus'
]);
$router->add('/logout', [
    'controller' => 'index',
    'action' => 'logout'
]);
$router->add('/changeTime', [
    'controller' => 'admin',
    'action' => 'changeTime'
]);
$router->add('/admin/latePage', [
    'controller' => 'admin',
    'action' => 'latePage'
]);
$router->add('/admin/lateUser', [
    'controller' => 'admin',
    'action' => 'late'
]);
$router->add('/admin/newStartJob', [
    'controller' => 'admin',
    'action' => 'newStartJob'
]);
$router->add('/admin/addHolidays', [
    'controller' => 'admin',
    'action' => 'addHolidays'
]);
$router->add('/admin/removeHoliday/{id}', [
    'controller' => 'admin',
    'action' => 'removeHoliday'
]);
$router->add('/admin/makeActiveHoliday', [
    'controller' => 'admin',
    'action' => 'makeActiveHoliday'
]);

$router->handle();
