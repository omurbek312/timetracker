{{ content() }}

<header>Admin Panel</header>
<a href="{{ url('/admin/createUser') }}">
    <button class="btn btn-primary">содать сотрудника</button>
</a>
<a href="{{ url('/') }}">
    <button class="btn btn-primary">переити к индексной странице</button>
</a>
<a href="{{ url('/admin/latePage') }}">
    <button class="btn btn btn-danger">Опаздавшие!</button>
</a>
<button class="btn btn-primary all_staffs">Все сотрудники</button>
<a href="{{ url('/admin/addHolidays') }}"><button class="btn btn-primary">Количество праздничныч дней</button></a>
<div class="container-fluid">
    <br>
    <table class="table table-striped users_table">
        <thead>
        <tr>
            <th scope="col">Имя сотрудников</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody class="staffs">
        </tbody>
    </table>
    <div class="search">
        <p>Найти сотрудника</p>
        <input type="search" id="searchName" class="form-control" placeholder="введите имя">
    </div>
</div>
<div class="date_filter">
    {{ form('admin','class':'form-group') }}
    <br>
    {{ form.render('month',['name':'month','type':'select','class':'form-control','id':'month','value':date_filter['month']]) }}
    <br>
    {{ form.render('year',['name':'year','type':'select','class':'form-control','id':'year','value':date_filter['year']]) }}
    <br>
    <input type="submit" class="btn btn-primary btn_reg d-none dateSubmit">
    </form>
</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">
            <button class="Show">Show/Hide</button>
        </th>

        {% for  user in users %}
            <td data-name="{{ user.name }}" scope="col">{{ user.name }}</td>
        {% endfor %}
    </tr>
    </thead>
    <tbody>

    {% for day in  count_days['days'] %}
        <tr {% if day == today %} id="today" class="toDaytd" {% else %} class="usersBoard"{% endif %}>
            <th scope="row">{{ day }}
                <br>
                <div class="days">{{ count_days['weeksDay'][day] }}</div>
            </th>

            {% for user in users %}
                <td data-name="{{ user.name }}">
                    {% for hours in hours_table %}
                        {% if hours['user'] == user.id and hours['day'] == day %}
                            <br>
                            <div class="time_div cla" id="{{ user.name }}/time">
                                <p>start: <span class="textTime"><input type="text" id="{{ hours['id'] }}"
                                                                        class="start_time"
                                                                        value="{{ hours['start_time'] }}"></span>
                                </p>
                                <p> stop: <span class="textTime"><input type="text" id="{{ hours['id'] }}"
                                                                        class="stop_time"
                                                                        value="{{ hours['stop_time'] }}"></span></p>
                            </div>
                        {% endif %}
                    {% endfor %}

                </td>
            {% endfor %}
        </tr>
    {% endfor %}
    </tbody>
</table>
{{ javascript_include('public/js/admin.js') }}
{{ javascript_include('public/js/date-table.js') }}
