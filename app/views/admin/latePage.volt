{{ content() }}
<body>
<header class="latePageheader">
    <div class="title_late_page">
        <h3>Список опаздавших</h3>
        <p>Введите желаемое время</p>
        <input type="text" id="time_check_late" placeholder="Формат 09:00">
    </div>

    <div class="late_check">
        <select class="form-control" id="late_time">
            <option value="09:00:00">09:00</option>
        </select>
    </div>
</header>
<div class="lateUsers">

</div>
</body>
{{ javascript_include('public/js/checkLateUser.js') }}