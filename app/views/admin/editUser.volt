{{ content() }}

{{ user.name }}
<body class="auth">
<div class="formReg container">
    <h3>Регистация</h3>
    {{ form('admin/user_edit/'~user.id,'class':'form-group') }}
    <br>
    {{ form.render('name',['name':'name','placeholder':'name','class':'form-control','value':user.name]) }}
    <br>
    {{ form.render('email',['name':'email','placeholder':'email','type':'email','class':'form-control','value':user.email]) }}
    <br>
    {{ form.render('role',['name':'role','type':'select','class':'form-control','value':user.role]) }}
    <br>
    {{ form.render('password',['name':'password','placeholder':'password','type':'password','class':'form-control','value':user.password]) }}
    <br>
    {% if user.status == 'true' %}
        <p>Статус : Активный</p>
    {% elseif user.status == 'false' %}
        <p>Статус : Не Активный</p>
    {% endif %}
    {{ form.render('status',['name':'status','type':'select','class':'form-control']) }}
    <br>
    <input type="submit" class="btn btn-primary btn_reg">
    </form>
</div>
</body>