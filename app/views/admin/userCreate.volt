{{ content() }}
<body class="auth">
<div class="formReg container">
    <h3>Регистация</h3>
    {{ form('admin/userCreate','class':'form-group') }}
    <br>
    {{ form.render('name',['name':'name','placeholder':'name','class':'form-control']) }}
    <br>
    {{ form.render('email',['name':'email','placeholder':'email','type':'email','class':'form-control']) }}
    <br>
    {{ form.render('role',['name':'role','type':'select','class':'form-control']) }}
    <br>
    {{ form.render('password',['name':'password','placeholder':'password','type':'password','class':'form-control']) }}
    <br>
    <input type="submit" class="btn btn-primary btn_reg">
    </form>
</div>
</body>