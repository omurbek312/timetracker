{{ content() }}
<div class="date_filter">
    {{ form('admin/addHolidays','class':'form-group') }}
    <br>
    {{ form.render('name',['name':'name','type':'text','class':'form-control','placeholder':'Введите названия']) }}
    {{ form.render('month',['name':'month','type':'select','class':'form-control']) }}
    {{ form.render('day',['name':'day','type':'number','class':'form-control','placeholder':'Введите день']) }}
    <br>
    <input type="submit" class="btn btn-primary btn_reg">
    </form>
</div>
<div class="holidays_board">
    {% for holiday in holidays %}
        <div class="holiday">

            {{ link_to('admin/removeHoliday/'~holiday.id, 'Удалить') }}

            <p> Название : {{ holiday.name }}</p>
            {% if  holiday.month == '01' %}
                <p> Месяц : Январь</p>
            {% endif %}

            {% if  holiday.month == '02' %}
                <p> Месяц : Февраль</p>
            {% endif %}

            {% if  holiday.month == '03' %}
                <p> Месяц : Март</p>
            {% endif %}

            {% if  holiday.month == '04' %}
                <p> Месяц : Апрель</p>
            {% endif %}

            {% if  holiday.month == '05' %}
                <p> Месяц : Май</p>
            {% endif %}

            {% if  holiday.month == '06' %}
                <p> Месяц : Июнь</p>
            {% endif %}
            {% if  holiday.month == '07' %}
                <p> Месяц : Июль</p>
            {% endif %}
            {% if  holiday.month == '08' %}
                <p> Месяц : Август</p>
            {% endif %}
            {% if  holiday.month == '09' %}
                <p> Месяц : Сентябрь</p>
            {% endif %}
            {% if  holiday.month == '10' %}
                <p> Месяц : Октябрь</p>
            {% endif %}
            {% if  holiday.month == '11' %}
                <p> Месяц : Ноябрь</p>
            {% endif %}
            {% if  holiday.month == '12' %}
                <p> Месяц : Декабрь</p>
            {% endif %}

            <p> день : {{ holiday.day }}</p>
            {% if holiday.status == 'true' %}
                <input type="checkbox" class="repeat" checked="checked" id="{{ holiday.id }}">
            {% else %}<input type="checkbox" class="repeat" id="{{ holiday.id }}"> {% endif %}
        </div>
    {% endfor %}
</div>
{{ javascript_include('public/js/admin.js') }}