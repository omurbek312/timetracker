<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phalcon PHP Framework</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->url->get('img/favicon.ico')?>"/>
        {{ stylesheet_link('public/css/styles.css') }}
        {{ stylesheet_link('public/bootstrap/css/bootstrap.css') }}
        {{ javascript_include('public/js/jquery/jquery.3.3min.js') }}
        {{ javascript_include('public/bootstrap-4.1.3-dis/js/bootstrap.js') }}

    </head>
    <body>
    {{ flash.output() }}
                    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            {{  content()}}
    </body>
</html>
