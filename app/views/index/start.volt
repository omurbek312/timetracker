{{ content() }}
<body class="auth">
<div class="formReg container">
    {{ form('/log-in','class':'form-group') }}
    <h3>Имя</h3>
    {{ form.render('name',['name':'name','class':'form-control','placeholder':'Введите имя']) }}
    <h3>Пароль</h3>
    {{ form.render('password',['name':'password','class':'form-control','placeholder':'Введите пароль','type':'password']) }}
    <br>
    <input type="submit" class="btn btn-primary btn_reg">
    </form>
</div>
</body>