{{ content() }}
{% set user_in_session = session.get('auth') %}
<a href="{{ url('logout') }}">
    <button class="logout btn btn btn-danger">Выйти</button>
</a>

{% if user_in_session['role'] == 'admin' %}
<a href="{{ url('admin') }}">
        <button type="button" class="btn btn-primary adminlink">Переити в админ понель?</button></a>{% endif %}
<h3>My Hours Log</h3>
<button class="test"></button>
<div class="data-hours">
    <p>You have : <span class="dataUser">{{ haveTotal }}</span></p>
    <p>Assigned :<span class="dataUser"> {{ Assigned }}</span></p>
    <p> Fails:</p>
</div>

<br>
<div class="date_filter">
    {{ form('/','class':'form-group') }}
    <br>
    {{ form.render('month',['name':'month','type':'select','class':'form-control','id':'month','value':date_filter['month']]) }}
    <br>
    {{ form.render('year',['name':'year','type':'select','class':'form-control','id':'year','value':date_filter['year']]) }}
    <br>
    <input type="submit" class="btn btn-primary btn_reg d-none dateSubmit">
    </form>
</div>

<table class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">
            <button class="Show">Show/Hide</button>
        </th>
        <th scope="col">{{ user_in_session['name'] }}</th>

        {% for  user in users %}
            {% if user.id != user_in_session['id'] %}
                <th scope="col">{{ user.name }}</th>
            {% endif %}
        {% endfor %}
    </tr>
    </thead>
    <tbody id="staff_body">

    {% for day in  count_days['days'] %}
        <tr {% if day == today %} class="toDaytd" {% else %}class="usersBoard"{% endif %}>
            <th id="today"{% if day == today %} class="toDaytd" {% else %} class="usersBoard"{% endif %}
                scope="row">{{ day }}
                <br>
                <div class="days">{{ count_days['weeksDay'][day] }}</div>
            </th>
            <td {% if day == today %} id="{{ day }}{{ user_in_session['id'] }}" {% endif %}>
                {% for hours in  hours_table %}
                    {% if hours['user'] == user_in_session['id'] and hours['day'] == day %}
                        <br>
                        <p><span class="textTime">start:{{ hours['start_time'] }}</span></p>
                        <br>
                        <p><span class="textTime">stop:{{ hours['stop_time'] }}</span></p>
                    {% endif %}
                {% endfor %}
                {% if day == today and check_date == simple_date and holiday =='false' %}
                    {% if status_button == 'start' %}
                        <button id="start" type="button" class="btn btn-success">start</button>
                    {% elseif  status_button == 'stop' %}
                        <button id="stop" type="button" class="btn btn-danger">stop</button>
                    {% endif %}
                {% endif %}
                {% for total in users_totals %}
                    {% if total['user'] == user_in_session['id'] and total['day'] == day  %}
                        <p>total : {{ total['total'] }}</p>
                    {% endif %}
                {% endfor %}
            </td>
            {% for user in users %}

                {% if user.id != user_in_session['id'] %}
                    <td {% if day == today %} class="toDaytd" {% else %}class="usersBoard"{% endif %}>
                        {% for hours in hours_table %}
                            {% if hours['user'] == user.id and hours['day'] == day and hours['day'] !=today %}
                                <br>
                                <p><span class="textTime">{{ hours['start_time'] }}</span></p>
                                <br>
                                <p><span class="textTime">{{ hours['stop_time'] }}</span></p>
                            {% endif %}
                        {% endfor %}
                        {% for total in users_totals %}
                            {% if total['user'] == user.id and total['day'] == day  %}
                                <p>total : {{ total['total'] }}</p>
                            {% endif %}
                        {% endfor %}
                    </td>
                {% endif %}
            {% endfor %}
        </tr>
    {% endfor %}
    </tbody>
</table>

<div class="table_session"></div>
{{ javascript_include('public/js/date-table.js') }}