<?php


use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;


class UserReg extends Form
{

    public function initialize($entity = null, $options = null)
    {
        $name = new Text('name');
        $this->add($name);
        $email = new Email('email');
        $this->add($email);
        $role = new Select('role');
        $role->addOption(['user' => 'User', 'admin' => 'Admin']);
        $this->add($role);
        $status = new Select('status');
        $status->addOption(['true' => 'Активировать', 'false' => 'Заблокировать']);
        $this->add($status);
        $password = new Password('password');
        $this->add($password);


    }
}