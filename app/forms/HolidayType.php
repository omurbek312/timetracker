<?php

use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;

class HolidayType extends Form
{
    public function initialize($entity = null, $options = null)
    {
        $name = new Text('name');
        $this->add($name);
        $month = new Select('month');
        $month->addOption([
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь']);
        $this->add($month);
        $day = new Numeric('day');
        $this->add($day);


    }
}