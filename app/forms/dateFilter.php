<?php

use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Form;

class dateFilter extends Form
{
    public function initialize($entity = null, $options = null)
    {
        $month = new Select('month');
        $month->addOption([
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь']);
        $this->add($month);
        $year = new Select('year');
        $year->addOption([
            '2015' => '2015',
            '2016' => '2016',
            '2017' => '2017',
            '2018' => '2018',
            '2019' => '2019',
            '20120' => '2020',
        ]);
        $this->add($year);

    }
}