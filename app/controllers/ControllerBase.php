<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
  public function _registerSession($user)
  {
    $this->session->set(
      "auth",
      [
        "id" => $user->id,
        "name" => $user->name,
        'role' => $user->role,
      ]
    );
  }

  public function getUser()
  {
    return $this->session->get('auth');
  }
}
