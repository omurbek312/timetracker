<?php

use Phalcon\Mvc\Controller;

class AdminController extends Controller
{

  public function usersAction()
  {
    $this->view->disable();
    $users = User::find();
    return json_encode($users);

  }

  public function editUserAction(int $id)
  {
    $form = new UserReg();
    $this->view->form = $form;
    if ($form->isValid()) {
      $editUser = new User();
      $password = $this->security->hash(md5($this->request->getPost('password')));
      $data = $this->request->getPost();
      $editUser->editUser($id, $data, $password);
      if ($this->request->isPost()) {
        return $this->response->redirect('admin');
      }
    }
    $user1 = User::findFirst($id);
    $this->view->setVars(['user' => $user1]);


  }

  public function removeAction($id)
  {
    $user = User::findFirst($id);
    $user->delete();
  }

  public function indexAction()
  {
    $ts = new \DateTime();
    $year = $ts->format('Y');
    $month = $ts->format('m');
    $dateFilterForm = new dateFilter();
    $this->view->form = $dateFilterForm;
    if ($this->request->isPost()) {
      $data = $this->request->getPost();;
      if ($dateFilterForm->isValid($data)) {
        $month = $data['month'];
        $year = $data['year'];
      }
    }
    $date = $year . '-' . $month . '-%';
    $today = $ts->format('d');
    $hoursMethods = new HoursData();
    $hours = HoursData::find(['date LIKE :date:', 'bind' => ['date' => $date]]);
    $users_hours = $hoursMethods->getUsersHours($hours);
    $users = User::find();
    $count_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $days = $hoursMethods->getCountDaysInMonth($count_days_in_month);
    $weekname = $hoursMethods->getWeekendsDay($month, $year, $days);
    $DaysInMont['days'] = $days;
    $DaysInMont['weeksDay'] = $weekname;
    return $this->view->setVars([
      'hours_table' => $users_hours,
      'users' => $users,
      'days' => $days,
      'date_filter' => $date_filter = ['month' => $month, 'year' => $year],
      'today' => $today,
      'count_days' => $DaysInMont]);
  }


  public function userCreateAction()
  {

    $form = new UserReg();
    $this->view->form = $form;
    $data = $this->request->getPost();
    if ($form->isValid($data)) {
      if ($this->request->isPost()) {
        $user = new User();
        $user = $user->userCreate($data, $this->security->hash(md5($data['password'])));
        if ($user) {
          return $this->response->redirect('admin');
        }
      }
    }

  }

  public function changeTimeAction()
  {
    if ($this->request->isAjax()) {
      $data = $this->request->getPost();
      $time_colculate = new HoursData();
      $hours = HoursData::findFirst([
        'id =:id:', 'bind' => ['id' => $data['id']]]);
      if ($data['status'] == 'start_time') {
        $hours->start_date = $data['start_time'];
        $stop_time = $hours->finish_date;
        $time_colculate = $time_colculate->hoursCalculate($data['start_time'], $stop_time);
        $hours->total = $time_colculate->format("%H:%i:%s");;
      } else if ($data['status'] == 'stop_time') {
        $hours->finish_date = $data['stop_time'];
        $time_colculate = $time_colculate->hoursCalculate($hours->start_date, $data['stop_time']);
        $hours->total = $time_colculate->format("%H:%i:%s");;
      }
      $hours->save();
    }
    $this->view->disable();
  }

  public function lateAction()
  {
    $date = date('Y-m-d');
    $hoursMethods = new HoursData();
    $this->view->disable();
    return json_encode($hoursMethods->getLateUser('09:00:00', $date));
  }

  public function latePageAction()
  {
  }

  public function newStartJobAction()
  {
    $date = date('Y-m-d');
    $hoursMethods = new HoursData();

    if ($this->request->isAjax()) {
      $time = $this->request->getPost();
      $this->view->disable();
      return json_encode($hoursMethods->getLateUser($time['time'], $date));
    }
  }

  public function addHolidaysAction()
  {
    $dateFilterForm = new HolidayType();
    $dateFilterForm->clear();
    $this->view->form = $dateFilterForm;
    if ($this->request->isPost()) {
      $data = $this->request->getPost();
      $holiday = new Holidays();
      $holiday->saveHoliday($data);
    }
    $holidaysList = Holidays::find();
    $this->view->setVar('holidays', $holidaysList);
  }

  public function removeHolidayAction($id)
  {
    $holiday = Holidays::findFirst($id);
    $holiday->delete();
    if ($holiday) {
      $this->flash->success('Удалено');
    }
    $this->response->redirect('admin/addHolidays');
  }

  public function makeActiveHolidayAction()
  {
    if ($this->request->isAjax()) {
      $data = $this->request->getPost();
      $holiday = Holidays::findFirst($data['id']);
      if ($data['checked'] == 'true') {
        $holiday->status = 'false';
      } else {
        $holiday->status = 'true';
      }
      $holiday->save();
    }
  }
}