<?php

class IndexController extends ControllerBase
{


  public function logoutAction()
  {
    $this->session->destroy();
    return $this->response->redirect('/log-in');
  }

  public function startAction()
  {

    $form = new AutgType();
    $this->view->form = $form;

    if ($this->request->isPost()) {
      $name = $this->request->getPost("name");
      $password = $this->request->getPost("password");
      $user = User::findFirst(['name = :name:', 'bind' => ['name' => $name]]);
      if ($user) {
        if ($this->security->checkHash(md5($password), $user->password) && $user->status == 'true') {
          $this->_registerSession($user);
          $this->response->redirect('/');
        } else {
          $this->flash->error(
            "Не верный пароль");
        }
      } else {
        $this->flash->error(
          "Пользователь не найден");
      }
    }
  }

  public function indexAction()
  {
    $year = date('Y');
    $month = date('m');
    $simpl_date = date('Y-m');
    $dateFilterForm = new dateFilter();
    $this->view->form = $dateFilterForm;
    if ($this->request->isPost()) {
      $data = $this->request->getPost();;
      if ($dateFilterForm->isValid($data)) {
        $month = $data['month'];
        $year = $data['year'];
      }
    }
    $filtrMonthEndYear = $year . '-' . $month . '-%';
    $filterDayChek = $year . '-' . $month;
    $user = $this->getUser();

    $method = new Users_total();
    $hoursMethods = new HoursData();
    $usersTotalForMonth = $method->GetTotalForMonth($filtrMonthEndYear, $user['id']);
    $status_button = $hoursMethods->checkStatusAction($user);
    $date_today = date('d');
    $hours = HoursData::find(["date LIKE :date:",
      "bind" => [
        'date' => $filtrMonthEndYear
      ]]);
    $users_hours = $hoursMethods->getUsersHours($hours);
    $count_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $days = $hoursMethods->getCountDaysInMonth($count_days_in_month);
    $weekname = $hoursMethods->getWeekendsDay($month, $year, $days);
    $sumHoursInMonth = $hoursMethods->hoursInMonth($weekname, $month);
    $users_total = $hoursMethods->getTotalsForDays($filtrMonthEndYear);
    $DaysInMont['days'] = $days;
    $DaysInMont['weeksDay'] = $weekname;
    $users = User::find();
    $holiday = new Holidays();
    $holiday = $holiday->CheckHoliday($month);
    $users->toArray();
    $this->view->setVars([
      'holiday' => $holiday,
      'users_totals' => $users_total,
      'simple_date' => $simpl_date,
      'check_date' => $filterDayChek,
      'date_filter' => $date_filter = ['month' => $month, 'year' => $year],
      'session' => $user,
      'users' => $users,
      'Assigned' => $sumHoursInMonth,
      'hours_table' => $users_hours,
      'count_days' => $DaysInMont,
      "today" => $date_today,
      'status_button' => $status_button,
      'nameDaysWeek' => $weekname,
      'haveTotal' => $usersTotalForMonth
    ]);

  }

  public function hoursSaveAction()

  {
    if ($this->request->isAjax()) {
      $user = $this->getUser();
      $data = $this->request->getPost();
      $status = $data['status'];
      $hoursMethods = new HoursData();
      if ($status == 'start') {
//
        $hoursMethods->saveStartTime($user['id']);
      } else if ($status == 'stop') {
        $hoursMethods->saveStopTime($user['id']);

      }
      $this->view->disable();
    }

  }


}