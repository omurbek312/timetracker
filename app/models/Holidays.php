<?php

use Phalcon\Mvc\Model;

class Holidays extends Model
{
  public function initialize()
  {
    $this->setSource("holidays");
  }

  public function saveHoliday($data)
  {
    $day = $data['day'];
    $holiday = new Holidays();
    $holiday->name = $data['name'];
    $holiday->month = $data['month'];
    $holiday->day = $day;
    $holiday->save();
  }

  public function getHolidaysCount()
  {
    return parent::findFirst(['date =:date:', 'bind' => date('Y-m-d')]);
  }

  public function CheckHoliday($month)
  {
    $holiday = Holidays::findFirst([
      'status = \'true\' AND month = :month: AND day =:today:',
      'bind' => [
        'month' => $month,
        'today' => date('d')
      ]]);
    if (!$holiday) {
      return 'false';
    } else {
      return 'true';
    }
  }
}