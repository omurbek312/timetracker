<?php

use Phalcon\Mvc\Model;

class HoursData extends Model
{
  public function initialize()
  {
    $this->setSource("hours_data");
  }

  public function hoursCalculate($start_time, $stop_time)

  {
    $startTime = new Datetime($start_time);
    $endTime = new DateTime($stop_time);
    return $endTime->diff($startTime);
  }

  public function getCountDaysInMonth($count_days_in_month)
  {

    $array = [];
    for ($i = 1; $i <= $count_days_in_month; $i ++) {
      if ($i < 10) {
        $array[] = $d = '0' . $i;
      } else if ($i >= 10) {
        $array[] = $i;
      }
    }

    return $array;
  }

  public function getWeekendsDay($month, $year, $days)
  {
    $nameDays = [];
    foreach ($days as $day) {

      $nameDays[$day] = strftime("%A", strtotime($month . '/' . $day . '/' . $year));
    }
    return $nameDays;
  }

  public function getUsersHours($hours)
  {
    $all_hours = [];
    foreach ($hours as $hours_data) {

      $date1 = $hours_data->date;
      $day = explode("-", $date1);
      $all_hours[] =
      $data_hours = [
        'id' => $hours_data->id,
        'start_time' => $hours_data->start_date,
        'stop_time' => $hours_data->finish_date,
        'day' => end($day),
        'date' => $day,
        'user' => $hours_data->user];
    }

    return $all_hours;
  }

  public function getTotalsForDays($date)
  {
    $users_total = Users_total::find(['date LIKE :date:', 'bind' => ['date' => $date]]);
    $all_totals = [];
    foreach ($users_total->toArray() as $total) {

      $date2 = $total['date'];
      $day = explode('-', $date2);
      $all_totals[] = ['id' => $total['id'], 'day' => end($day), 'user' => $total['user'], 'total' => $total['total'], 'date' => $total['date']];

    }
    return $all_totals;
  }

  public function checkStatusAction($user)
  {
    $hoursChek = HoursData::findFirst(['(user = :user:) AND finish_date IS NULL order by id ASC', 'bind' => ['user' => $user['id']]]);

    if ($hoursChek == false) {
      $status = 'start';
    } else {
      $status = 'stop';
    }
    return $status;
  }

  public function hoursInMonth($DaysInMonth, $month)
  {
    $holidays = Holidays::find(['status = \'true\' AND month =:month:', 'bind' => ['month' => $month]]);
    $holidays = $holidays->toArray();
    $holidays_count = count($holidays) * 8;
    $array = [];
    foreach ($DaysInMonth as $day) {
      if ($day == 'Saturday' || $day == 'Friday') {
        continue;
      }
      $array[] = $day;
    }
    $HoursSumInMonth = count($array) * 8;
    $result = $HoursSumInMonth - $holidays_count;
    return $result;

  }

  public function getLateUser($timeStartJob, $date)
  {
    $hours = parent::find(['date =:date: AND first = :true:', 'bind' => ['date' => $date, 'true' => 'true']]);
    $timeStartJob1 = strtotime($timeStartJob);
    $LateUsers = [];
    foreach ($hours->toArray() as $time) {
      $start_time = strtotime($time['start_date']);
      if ($timeStartJob1 < $start_time) {
        $LateUsers[] = $time;
      }
    }
    $users = User::find();
    if (empty($LateUsers)) {
    } else {
      $lateUsers = [];
      foreach ($users->toArray() as $user) {
        foreach ($LateUsers as $LateUser) {
          if ($LateUser['user'] == $user['id']) {
            $difference = $this->hoursCalculate($timeStartJob, $LateUser['start_date']);
            $lateUsers[] = ['user' => $user, 'time' => $LateUser['start_date'], 'difference' => $difference->format("%H:%i:%s")];
          }
        }
      }

      return $lateUsers;
    }
  }

  public function checkFirsTime($date, $user)
  {
    $checkFirstHours = HoursData::findFirst([
      'date =:date: AND user=:user: ',
      'bind' => [
        'date' => $date,
        'user' => $user
      ]]);
    return $checkFirstHours;
  }

  public function saveStartTime($user)
  {
    $date = date('Y-m-d');
    $hours = new HoursData();
    $stop_time = date('H:i:s');
    if (!$this->checkFirsTime($date, $user)) {
      $hours->first = 'true';
    }
    $hours->start_date = $stop_time;
    $hours->date = $date;
    $hours->user = $user;
    $hours->save();
  }

  public function saveStopTime($user)
  {
    $date = date('Y-m-d');
    $stop_time = date('H:i:s');
    $hours = HoursData::findFirst([
      "((user = :user:) AND finish_date IS NULL) AND date =:date:",
      "bind" => [
        "user" => $user,
        "date" => $date
      ]
    ]);


    $start_time = $hours->start_date;
    $result_calculate = $this->hoursCalculate($start_time, $stop_time);
    $hours->finish_date = $stop_time;
    $hours->total = $result_calculate->format("%H:%i:%s");
    $hours->user = $user;
    $hours->save();
    $this->saveTotalTimesThisDay($user);

  }

  public function calculateTotalTimeForDay($data)
  {
    $total = [];
    foreach ($data as $date) {
      $total[] = $date['total'];
    }


    $total = array_map(function ($t) {
      [$h, $i, $s] = explode(':', $t);
      return ($h * 3600) + ($i * 60) + $s;
    }, $total);

    $timestamp = array_sum($total);
    $total = new DateTime("@$timestamp");
    $total = $total->format('H:i:s');
    return $total;
  }

  public function saveTotalTimesThisDay($user)
  {
    $simpleDate = date('Y-m-d');
    $hours = HoursData::find(['((date =:date:)AND user = :user:) order by id ASC',
      'bind' => ['date' => $simpleDate, 'user' => $user]
    ]);
    $data = $hours->toArray();
    $total = $this->calculateTotalTimeForDay($data);
    $users_totalCheck = Users_total::findFirst(
      ['(user =:user:) AND date =:date:', 'bind' => ['user' => $user, 'date' => $simpleDate]]
    );
    if ($users_totalCheck == false) {

      $users_total = new Users_total();
      $users_total->user = $user;
      $users_total->total = $total;
      $users_total->date = $simpleDate;
      $users_total->save();
    } else {
      $users_totalCheck->total = $total;
      $users_totalCheck->save();
    }
  }
}