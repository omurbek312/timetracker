<?php

use Phalcon\Mvc\Model;

class User extends Model
{


  public function userCreate($data, $password)
  {
    if (!empty($data)) {
      $user = new User();
      $user->name = $data['name'];
      $user->password = $password;
      $user->email = $data['email'];
      $user->role = $data['role'];
      $user->status = $data['status'];
      $user = $user->save();
      return $user;
    }
  }

  public function editUser($id,$data,$password)
  {
    $user = User::findFirst($id);
    $user->name = $data['name'];
    $user->email = $data['email'];
    $user->role = $data['role'];
    $user->status = $data['status'];
    $user->password = $password;
    $user->save();
  }


}