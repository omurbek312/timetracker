<?php

use Phalcon\Mvc\Model;

class Users_total extends Model
{
  public function initialize()
  {
    $this->setSource("users_total");
  }

  public function GetTotalForMonth($filtrMonthEndYear, $user)
  {
    $method = new HoursData();
    $user_total = Users_total::find(
      ['(user = :user:) AND date LIKE :date:',
        'bind' =>
          [
            'user' => $user,
            'date' => $filtrMonthEndYear]
      ]);
    $user_total = $user_total->toArray();
    return $method->calculateTotalTimeForDay($user_total);

  }
}